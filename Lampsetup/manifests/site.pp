class { 'apache': }
class { 'apache::mod::rewrite': }
class { 'apache::vhosts': }
class { 'apache::mod::php': }
class { 'mysql::server': }
mysql_user { 'test@localhost':
  ensure => 'present',
  password_hash => mysql_password(test),
}
mysql_grant { 'test@localhost/*.*':
  ensure     => 'present',
  options    => ['GRANT'],
  privileges => ['ALL'],
  table      => '*.*',
  user       => 'test@localhost',
}

